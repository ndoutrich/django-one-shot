# from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from todos.models import TodoList, TodoItem

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["todos"] = TodoList.objects.all()
        return context


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["tasks"] = TodoItem.objects.all()
    #     return context


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]

    def form_valid(self, form):
        list = form.save(commit=False)
        list.owner = self.request.user
        list.save()
        form.save_m2m()
        return redirect("todo_list_detail", pk=list.id)


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def form_valid(self, form):
        list = form.save(commit=False)
        list.owner = self.request.user
        list.save()
        form.save_m2m()
        return redirect("todo_list_detail", pk=list.id)


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/items/new.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])

    # def form_valid(self, form):
    #     list = form.save(commit=False)
    #     list.owner = self.request.user
    #     list.save()
    #     form.save_m2m()
    #     return redirect("todo_list_detail", pk=list.id)


# def create_todo_item(request):
#     task = request.POST.get("task")


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
